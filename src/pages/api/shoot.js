const axios = require('axios');
const fs = require('fs/promises');

export default async function handler(req, res) {
  const dir = process.env.DOCKER ? `${process.cwd()}/inputs` : `${process.cwd()}/inputs/Smart\ Shooter\ 4`;
  await fs.readdir(dir).then((f) => Promise.all(f.map(e => fs.unlink(`${dir}/${e}`))))

  return axios
    .get(
      process.env.DOCKER
        ? "http://photobooth-zmq:4567/shoot"
        : "http://localhost:4567/shoot"
    )
    .then(function (response) {
      res.status(200).json({ success: true });
    })
    .catch(function (error) {
      res.status(200).json({ success: false, error: !error.status ? 'Serivce offline' : error.message });
    })
}
