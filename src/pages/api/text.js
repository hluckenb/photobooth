const client = require("twilio")(process.env.TWILIO_ACCOUNT_SID, process.env.TWILIO_ACCOUNT_TOKEN);

export default async function handler(req, res) {
  if (req.method !== 'POST') {
    res.status(405).send({ message: 'Only POST requests allowed' })
    return
  }

  let url = `https://res.cloudinary.com/dxrkyigpx/video/upload/fl_attachment/v1675740283/${req.body.publicId}.mp4`;

  return client.messages
    .create({
      body: `Looking good! 💐\nClick to download your video from Give Them Their Flowers and don’t forget to tag #GTTF when you share.\n${url}`,
      from: "+18559635572",
      to: req.body.phone
    })
    .then(message => {
      res.status(200).json({ success: true, sid: message.sid, url });
    })
    .catch(error => {
      res.status(200).json({ success: false, error });
    });
}
