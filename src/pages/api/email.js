const formData = require("form-data");
const Mailgun = require("mailgun.js");
const mailgun = new Mailgun(formData);

export default async function handler(req, res) {
  if (req.method !== "POST") {
    res.status(405).send({ message: "Only POST requests allowed" });
    return;
  }

  let url = `https://res.cloudinary.com/dxrkyigpx/video/upload/fl_attachment/v1675740283/${req.body.publicId}.mp4`;

  const mg = mailgun.client({
    username: "api",
    key: process.env.MAILGUN_API_KEY
  });

  let messageParams = {
    from: "Mxxd Media <noreply@mxxdmedia.link>",
    to: [req.body.email],
    subject: "Hi! Your video is ready.",
    text: `Looking good! 💐\nClick to download your video from Give Them Their Flowers and don’t forget to tag #GTTF when you share.\n${url}`
  };

  mg.messages
    .create("mg.mxxdmedia.link", messageParams)
    .then(response => {
      res.status(200).json({ success: true, response: response });
    })
    .catch(error => {
      res.status(200).json({ success: false, error });
    });
}
