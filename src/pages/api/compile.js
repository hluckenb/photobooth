const cloudinary = require("cloudinary").v2;
const ffmpeg = require("fluent-ffmpeg");
const fs = require("fs");
const glob = require("glob");

cloudinary.config({
  secure: true
});

export default async function handler(req, res) {
  const inputs = process.env.DOCKER
    ? `${process.cwd()}/inputs`
    : `${process.cwd()}/inputs/Smart\ Shooter\ 4`;

  glob(`${inputs}/*.JPG`, (err, files) => {
    files.forEach((file, index) => {
      const newPath = `${inputs}/SSP_${(index + 1)
        .toString()
        .padStart(5, "0")}.JPG`;

      fs.rename(file, newPath, error => {});
    });
  });

  const myPromise = new Promise((resolve, reject) => {
    ffmpeg()
      .input(`${inputs}/SSP_%05d.JPG`)
      .inputOptions(["-framerate 6", "-f image2", "-start_number 1"])
      .input(`${process.cwd()}/overlay.png`)
      .complexFilter(
        "[0:v]pad=iw:2*trunc(iw*16/9/2):(ow-iw)/2:(oh-ih)/2[v0];[1:v][v0]scale2ref[v1][v0];[v0][v1]overlay=(W-w)/2:(H-h)/2"
      )
      .videoCodec("libx264")
      .outputOptions([
        "-loop -1",
        "-preset veryfast",
        "-profile:v baseline",
        "-level 3.0"
      ])
      .save(`${process.cwd()}/public/video.mp4`)
      .on("end", function (stdout, stderr) {
        cloudinary.uploader
          .upload(`${process.cwd()}/public/video.mp4`, {
            resource_type: "video"
          })
          .then(result => {
            resolve(result.public_id);
          })
          .catch(error => {
            reject(error)
          });
      })
      .on("error", function (err, stdout, stderr) {
        reject(err.message);
      });
  });

  return myPromise
    .then(publicId => {
      res.status(200).json({
        success: true,
        publicId
      });
    })
    .catch(error => {
      res.status(200).json({ success: false, error: error });
    });
}
