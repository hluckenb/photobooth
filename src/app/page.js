"use client";

import { useRouter } from "next/navigation";

export default function Home() {
  const router = useRouter();

  return (
    <>
      <div className="flex items-center justify-center h-screen">
        <button
          className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-4 px-12 rounded text-5xl"
          onClick={() => router.push("/countdown")}
        >
          Start
        </button>
      </div>
    </>
  );
}
