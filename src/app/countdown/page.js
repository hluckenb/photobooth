"use client";

import * as React from "react";
import Countdown from "react-countdown";
import Link from "next/link";
import { useRouter } from "next/navigation";

export default function CountdownRoute() {
  const router = useRouter();

  return (
    <div className="flex items-center justify-center h-screen text-white">
      <div>
        <div className="text-9xl mb-10">
          <Countdown
            date={Date.now() + 5000}
            intervalDelay={0}
            precision={3}
            renderer={props => <div>{`${props.seconds + 1}...`}</div>}
            onComplete={() => router.push("/take_photos")}
          />
        </div>
        <div className='text-center'>
          <Link href="/">&larr; Cancel</Link>
        </div>
      </div>
    </div>
  );
}
