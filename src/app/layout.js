import './globals.css'

export default function RootLayout({ children }) {
  return (
    <html lang="en">
      <head />
      <body className='bg-slate-900'>{children}</body>
    </html>
  )
}
