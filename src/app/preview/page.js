"use client";

import * as React from "react";
import dynamic from "next/dynamic";
import Link from "next/link";
import PhoneInput from "react-phone-number-input/input";
import { isPossiblePhoneNumber } from "react-phone-number-input";
import { useForm, Controller } from "react-hook-form";
import useAxios from "axios-hooks";
import { useSearchParams, useRouter } from "next/navigation";

const VideoPlayer = dynamic(() => import("./player"), { ssr: false });

export default function Preview() {
  const router = useRouter();
  const playerRef = React.useRef();
  const searchParams = useSearchParams();

  const [
    { data: phoneData, loading: phoneLoading, error: phoneError },
    executePhone
  ] = useAxios(
    {
      url: "/api/text",
      method: "POST"
    },
    { manual: true }
  );

  const [
    { data: emailData, loading: emailLoading, error: emailError },
    executeEmail
  ] = useAxios(
    {
      url: "/api/email",
      method: "POST"
    },
    { manual: true }
  );

  const {
    handleSubmit,
    formState: { errors },
    control
  } = useForm();

  const {
    register,
    handleSubmit: handleSubmitEmail,
    formState: { errors: emailErrors },
    control: emailControl
  } = useForm();

  const onPhoneSubmit = data => {
    executePhone({
      data: {
        phone: data["phone-input"],
        publicId: searchParams.get("publicId")
      }
    })
      .then(function (response) {
        setTimeout(function () {
          router.push("/");
        }, 3000);
      })
      .catch(function (error) {
        setTimeout(function () {
          location.reload();
        }, 3000);
      });
  };

  const onEmailSubmit = data => {
    executeEmail({
      data: {
        email: data["email-input"],
        publicId: searchParams.get("publicId")
      }
    })
      .then(function (response) {
        setTimeout(function () {
          router.push("/");
        }, 3000);
      })
      .catch(function (error) {
        setTimeout(function () {
          location.reload();
        }, 3000);
      });
  };

  const handlePhoneValidate = value => {
    if (value === undefined || value === null) {
      return false;
    }

    return isPossiblePhoneNumber(value);
  };

  const handleEmailValidate = value => {
    if (value === undefined || value === null) {
      return false;
    }

    const regex =
      /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

    return regex.test(value);
  };

  if (
    phoneData ||
    phoneLoading ||
    phoneError ||
    emailData ||
    emailLoading ||
    emailError
  ) {
    return (
      <div className="flex items-center justify-center h-screen text-white">
        <div>
          <div className="text-5xl mb-10">
            {(phoneLoading || emailLoading) && "Sending..."}
            {(phoneData || emailData) && "Sent!"}
            {(phoneError || emailError) && "Error sending, please try again."}
          </div>
        </div>
      </div>
    );
  }

  return (
    <div className="flex items-center justify-center h-screen text-white">
      <div>
        <VideoPlayer
          playerRef={playerRef}
          url={`https://res.cloudinary.com/dxrkyigpx/video/upload/fl_attachment/v1675740283/${searchParams.get(
            "publicId"
          )}.mp4`}
        />
        <div className="w-full mt-10 text-center">
          Send to your phone or email:
        </div>
        <div className="mt-5 w-6/12 inline-block pr-4">
          <form onSubmit={handleSubmit(onPhoneSubmit)}>
            <div>
              <label
                className="block text-white text-sm font-bold mb-2"
                htmlFor="phone"
              >
                Phone Number
              </label>
              <Controller
                name="phone-input"
                control={control}
                rules={{
                  required: true,
                  validate: value => handlePhoneValidate(value)
                }}
                render={({ field: { onChange, value } }) => (
                  <PhoneInput
                    id="phone"
                    className="shadow appearance-none border rounded py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline w-full"
                    country="US"
                    value={value}
                    onChange={onChange}
                  />
                )}
              />
              {errors["phone-input"] && (
                <p className="error-message">Invalid Phone</p>
              )}
            </div>
            <div className="basis-1/4 flex flex-col items-center justify-center">
              <button
                className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline mt-4"
                type="submit"
              >
                Send
              </button>
            </div>
          </form>
        </div>
        <div className="mt-10 w-6/12 inline-block pl-4">
          <form onSubmit={handleSubmitEmail(onEmailSubmit)}>
            <div>
              <label
                className="block text-white text-sm font-bold mb-2"
                htmlFor="email"
              >
                Email
              </label>
              <input
                id="email"
                className="shadow appearance-none border rounded py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline w-full"
                {...register("email-input", {
                  required: true,
                  validate: value => handleEmailValidate(value)
                })}
              />
              {emailErrors["email-input"] && (
                <p className="error-message">Invalid Email</p>
              )}
            </div>
            <div className="basis-1/4 flex flex-col items-center justify-center">
              <button
                className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline mt-4"
                type="submit"
              >
                Send
              </button>
            </div>
          </form>
        </div>
        <div className="text-center mt-10">
          <Link href="/">&larr; Start Over</Link>
        </div>
      </div>
    </div>
  );
}
