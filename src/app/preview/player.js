import ReactPlayer from "react-player/lazy";

export default function VideoPlayer({ playerRef, url }) {
  return (
    <ReactPlayer
      ref={playerRef}
      url={url}
      muted
      playing
      loop
      height="650px"
    />
  );
}
