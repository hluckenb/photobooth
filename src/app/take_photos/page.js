"use client";

import * as React from "react";
import useAxios from "axios-hooks";
import { useRouter } from "next/navigation";

export default function TakePhotos() {
  const router = useRouter();
  const [text, setText] = React.useState('Taking Photos...')
  const [{}, execute] = useAxios("/api/shoot", {
    useCache: false
  })

  React.useEffect(() => {
    execute().then(resp => {
      setText('Done.')
      setTimeout(() => {
        router.push('/compile')
      }, "3000")
    });
  }, [execute]);

  return (
    <div className="flex items-center justify-center h-screen text-white">
      <div>
        <div className="text-5xl mb-10">
          {text}
        </div>
      </div>
    </div>
  );
}
