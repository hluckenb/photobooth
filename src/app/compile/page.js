"use client";

import * as React from "react";
import useAxios from "axios-hooks";
import { useRouter } from "next/navigation";

export default function Compile() {
  const router = useRouter();
  const [text, setText] = React.useState("Compiling Photos...");
  const [{}, execute] = useAxios("/api/compile", {
    useCache: false
  });

  React.useEffect(() => {
    execute().then(resp => {
      setText("Done.");
      setTimeout(() => {
        router.push(`/preview?publicId=${resp.data.publicId}`);
      }, "3000");
    });
  }, [execute]);

  return (
    <div className="flex items-center justify-center h-screen text-white">
      <div>
        <div className="text-5xl mb-10">{text}</div>
      </div>
    </div>
  );
}
